from PyQt5 import QtWidgets
from Interfejs import Ui_MainWindow
from PyQt5.QtWidgets import QMessageBox, QGraphicsScene
from PyQt5.QtGui import QPixmap
import sys

class mywindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(mywindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.lepath.setFocus()
        self.ui.pbload.clicked.connect(self.akcja)
        self.ui.pbremove.clicked.connect(self.akcja)
        self.ui.lepath.setText("C:/Users/Oliwia Drozdek/Pictures/MOJE NAJLEPSZE ZDJĘCIA/")
        self.scene=QGraphicsScene()

    def akcja(self):
        try:
            nadawca=self.sender()
            if nadawca.text()=='Załaduj':
                nazwa=self.ui.lepath.text()
                pixmap=QPixmap()
                pixmap.load(nazwa)
                h=self.ui.gvphoto.height()
                w=self.ui.gvphoto.width()
                self.scene.addPixmap(pixmap.scaled(w,h))
                self.ui.gvphoto.setScene(self.scene)

            elif nadawca.text()=='Usuń':
                self.ui.lepath.clear()

        except ValueError:
            QMessageBox.warning(self, "Błąd","Nie udało się wczytać zdjęcia.", QMessageBox.Ok)

app = QtWidgets.QApplication(sys.argv)
application = mywindow()
application.show()
sys.exit(app.exec())

