# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'APIprzetwarzanie_obrazu.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setEnabled(True)
        MainWindow.resize(729, 501)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMaximumSize(QtCore.QSize(16777215, 12000000))
        MainWindow.setLayoutDirection(QtCore.Qt.LeftToRight)
        MainWindow.setAutoFillBackground(False)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setSizeConstraint(QtWidgets.QLayout.SetMinAndMaxSize)
        self.gridLayout.setContentsMargins(6, 6, 6, 80)
        self.gridLayout.setSpacing(20)
        self.gridLayout.setObjectName("gridLayout")
        self.gvphoto = QtWidgets.QGraphicsView(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.gvphoto.sizePolicy().hasHeightForWidth())
        self.gvphoto.setSizePolicy(sizePolicy)
        self.gvphoto.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.gvphoto.setObjectName("gvphoto")
        self.gridLayout.addWidget(self.gvphoto, 0, 1, 1, 1)
        self.twtools = QtWidgets.QTabWidget(self.centralwidget)
        self.twtools.setObjectName("twtools")
        self.tab1 = QtWidgets.QWidget()
        self.tab1.setObjectName("tab1")
        self.twtools.addTab(self.tab1, "")
        self.tab2 = QtWidgets.QWidget()
        self.tab2.setObjectName("tab2")
        self.twtools.addTab(self.tab2, "")
        self.gridLayout.addWidget(self.twtools, 0, 0, 1, 1)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSizeConstraint(QtWidgets.QLayout.SetMaximumSize)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.lepath = QtWidgets.QLineEdit(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily("Verdana")
        font.setPointSize(10)
        self.lepath.setFont(font)
        self.lepath.setObjectName("lepath")
        self.horizontalLayout.addWidget(self.lepath)
        self.pbload = QtWidgets.QPushButton(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily("Verdana")
        font.setPointSize(9)
        self.pbload.setFont(font)
        self.pbload.setAutoFillBackground(True)
        self.pbload.setObjectName("pbload")
        self.horizontalLayout.addWidget(self.pbload)
        self.pbremove = QtWidgets.QPushButton(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily("Verdana")
        font.setPointSize(10)
        self.pbremove.setFont(font)
        self.pbremove.setObjectName("pbremove")
        self.horizontalLayout.addWidget(self.pbremove)
        self.gridLayout.addLayout(self.horizontalLayout, 1, 1, 1, 1)
        self.gridLayout.setColumnStretch(0, 1)
        self.gridLayout.setColumnStretch(1, 3)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 729, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)

        self.retranslateUi(MainWindow)
        self.twtools.setCurrentIndex(1)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Przetwarzanie zdjęcia"))
        self.twtools.setTabText(self.twtools.indexOf(self.tab1), _translate("MainWindow", "Tab 1"))
        self.twtools.setTabText(self.twtools.indexOf(self.tab2), _translate("MainWindow", "Tab 2"))
        self.pbload.setText(_translate("MainWindow", "Załaduj"))
        self.pbremove.setText(_translate("MainWindow", "Usuń"))


